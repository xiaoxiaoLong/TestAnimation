//
//  AppDelegate.h
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

