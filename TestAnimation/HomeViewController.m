//
//  ViewController.m
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title= @"动画集结";
    
    [self getData];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
 }

-(void)getData
{
    data = @[@"basic (view)",@"basic (subView)",@"dynamic",@"Core Animation",@"pop",@"text",@"image(Core Animation 2)",@"Loading",@"Cart",@"转场",@"Path & Frame",@"Sprite",@"Drawing",@"other"];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(!cell)
    {
        cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    NSString *text = data[indexPath.row];
    cell.textLabel.text = text;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger index = indexPath.row;
    NSLog(@"indexpath is %d",indexPath.row);
    
    if(index == 0)
    {
        [self performSegueWithIdentifier:@"basic" sender:nil];
    }else if ( index == 1)
    {
        [self performSegueWithIdentifier:@"basic2" sender:nil];
    }else if (index == 2)
    {
        [self performSegueWithIdentifier:@"dynamic" sender:nil];
    }
    else if (index == 3)
    {
        [self performSegueWithIdentifier:@"Share of Core Animation" sender:nil];
    }
    else if ( index == 4)
    {
        // pop
        MasterViewController *vc = [[MasterViewController alloc] init];
        [self.navigationController pushViewController:vc
                                             animated:YES];
        
    }
    else if ( index == 5) //TEXT
    {
        [self performSegueWithIdentifier:@"Label" sender:nil];
    }
    else if ( index == 6)//image
    {
        UIStoryboard *story2 = [UIStoryboard storyboardWithName:@"CoreAnimationFunHouse" bundle:[NSBundle mainBundle]];
        UITableViewController *tvc = [story2 instantiateViewControllerWithIdentifier:@"CoreAnimationFunHouse.storyboard"];
        [self.navigationController pushViewController:tvc animated:YES];
    }
    else if (index == 7)
    {
        [self performSegueWithIdentifier:@"goto Other" sender:nil];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if( [segue.identifier isEqualToString:@"Share of Core Animation"])
    {
        ShareOfCoreAnimation *vc =(ShareOfCoreAnimation*) segue.destinationViewController;
        vc.title = @"Share of Core Animation";
    }
    
}

@end
