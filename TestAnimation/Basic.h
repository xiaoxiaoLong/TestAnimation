//
//  Basic.h
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Basic : UIViewController
{
    UIImageView *blueView;
    UIImageView *greenView;
    
    NSInteger typeID;

}

@property (nonatomic, retain) IBOutlet UIImageView *blueView;
@property (nonatomic, retain) IBOutlet UIImageView *greenView;

@property (nonatomic, assign) NSInteger typeID;

- (IBAction)buttonPressed1:(id)sender;
- (IBAction)buttonPressed2:(id)sender;

@end
