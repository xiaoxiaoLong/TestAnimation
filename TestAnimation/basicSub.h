//
//  basicSub.h
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface basicSub : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewTest;


- (IBAction)fade:(UIButton *)sender;

- (IBAction)rotate:(UIButton *)sender;
- (IBAction)move:(UIButton *)sender;
- (IBAction)scale:(UIButton *)sender;
- (IBAction)shake:(UIButton *)sender;
- (IBAction)all:(UIButton *)sender;
- (IBAction)threeD:(UIButton *)sender;

@end
