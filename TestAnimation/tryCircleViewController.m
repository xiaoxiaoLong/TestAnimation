//
//  tryCircleViewController.m
//  learnDynamic
//
//  Created by Open on 14-7-8.
//  Copyright (c) 2014年 com.open.Carlipa. All rights reserved.
//


// 01 不断改变 的 方向
// 02 参考  牛顿
// 03 吸附的 本身好像也是可以的
//
//



#import "tryCircleViewController.h"

@interface tryCircleViewController ()
{
    UIView *view;
    UIPushBehavior *_userDragBehavior;

}

@property (nonatomic,retain) UIDynamicAnimator *animator;
@property (nonatomic,retain) UICollisionBehavior *collision;

@end

@implementation tryCircleViewController


@synthesize animator;
@synthesize collision;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)createTheView
{
    animator=[[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    
    view=[[UIView alloc] initWithFrame:CGRectMake(150, 300, 60, 60)];
    view.tag=100;
    view.backgroundColor=[UIColor grayColor];
    
    UILabel *testL=[[UILabel alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
    testL.backgroundColor=[UIColor clearColor];
    testL.text = @"1";
    
    UIFontDescriptor *fd=[[UIFontDescriptor alloc] initWithFontAttributes:@{}];
    
    testL.font=[UIFont fontWithDescriptor:fd size:25];
    testL.textColor=[UIColor blackColor];
    [view addSubview:testL];
    
    view.layer.cornerRadius=30;
    view.clipsToBounds=YES;
    
    [self.view addSubview:view];

    
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleBallBearingPan:)];
    [view addGestureRecognizer:gesture];
    
    
    UIView *view2=[[UIView alloc] init];
    CGPoint viewCenter=view.center;
    
    [view2 setFrame:CGRectMake(viewCenter.x-10, viewCenter.y+60, 20, 20)];
    view2.layer.cornerRadius=10.0f;
    view2.clipsToBounds = YES;
    view2.backgroundColor=[UIColor yellowColor];
    [self.view addSubview:view2];
    
    
     UIPanGestureRecognizer *gesture2 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleBallBearingPan:)];
    [view2 addGestureRecognizer:gesture2];
    
    
    UIAttachmentBehavior *atb=[[UIAttachmentBehavior alloc] initWithItem:view2 attachedToItem:view];
    [animator addBehavior:atb];
    
    UIView *view3=[[UIView alloc] init];
    [view3 setFrame:CGRectMake(viewCenter.x-10, viewCenter.y-80, 20, 20)];
    view3.layer.cornerRadius=10.0f;
    view3.clipsToBounds = YES;
    view3.backgroundColor=[UIColor yellowColor];
    [self.view addSubview:view3];
    
    
    UIPanGestureRecognizer *gesture3 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleBallBearingPan:)];
    [view3 addGestureRecognizer:gesture3];
    //UIView *newView = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:view3]];
    
    UIAttachmentBehavior *atb2=[[UIAttachmentBehavior alloc] initWithItem:view3 attachedToItem:view];
    [animator addBehavior:atb2];
    
    collision=[[UICollisionBehavior alloc] initWithItems:@[view,view2,view3,self.view]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
//    [self.animator addBehavior:collision];
}

#pragma mark - UIGestureRecognizer target method
- (void)handleBallBearingPan:(UIPanGestureRecognizer *)recognizer
{
    // If we're starting the gesture then create a drag force
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if(_userDragBehavior) {
            [animator removeBehavior:_userDragBehavior];
        }
        _userDragBehavior = [[UIPushBehavior alloc] initWithItems:@[recognizer.view] mode:UIPushBehaviorModeContinuous];
        [animator addBehavior:_userDragBehavior];
    }
    
    // Set the force to be proportional to distance the gesture has moved
    _userDragBehavior.pushDirection = CGVectorMake([recognizer translationInView:self.view].x / 10.f, [recognizer translationInView:self.view].y / 10.f);
    
    // If we're finishing then cancel the behavior to 'let-go' of the ball
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [animator removeBehavior:_userDragBehavior];
        _userDragBehavior = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   

}

-(void)viewDidLayoutSubviews
{
    [self createTheView];
    
    [self applyDynamicBehaviors];
}

#pragma mark - UIDynamics utility methods
- (void)applyDynamicBehaviors
{
    // Create the composite behavior. This will contain the different child behaviors
    UIDynamicBehavior *behavior = [[UIDynamicBehavior alloc] init];
    
    // Attach each ball bearing to it's pivot point
  
        UIDynamicBehavior *attachmentBehavior = [self createAttachmentBehaviorForBallBearing:view];
        [behavior addChildBehavior:attachmentBehavior];
     
    // Apply gravity to the ball bearings
   // [behavior addChildBehavior:[self createGravityBehaviorForObjects:@[view]]];
    
    // Apply collision behavior to the ball bearings
    [behavior addChildBehavior:[self createCollisionBehaviorForObjects:@[view]]];
    
    // Apply correct dynamic item behavior
    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[view]];
    // Elasticity governs the efficiency of the collisions
    itemBehavior.elasticity = 1.0;
    itemBehavior.allowsRotation = NO;
    itemBehavior.resistance = 0.f;
    [behavior addChildBehavior:itemBehavior];
    
    // Create the animator
//    animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    // Add the composite behavior
    [animator addBehavior:behavior];
}

- (UIDynamicBehavior *)createAttachmentBehaviorForBallBearing:(id<UIDynamicItem>)ballBearing
{
    CGPoint anchor = ballBearing.center;
    // The anchor point is vertically above the ball bearing
    anchor.y -= CGRectGetHeight(self.view.frame) / 4.0;
    
    // Draw a box at the anchor. This isn't part of the behavior but is good visually
    UIView *blueBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
    blueBox.backgroundColor = [UIColor blueColor];
    blueBox.center = anchor;
    [self.view addSubview:blueBox];
    
    // Create the attachment behavior
//    UIAttachmentBehavior *behavior = [[UIAttachmentBehavior alloc] initWithItem:ballBearing
//                                                               attachedToAnchor:anchor];
    
    UIAttachmentBehavior *behavior = [[UIAttachmentBehavior alloc] initWithItem:ballBearing
                                                               attachedToAnchor:ballBearing.center];
    return behavior;
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIDynamicBehavior *)createGravityBehaviorForObjects:(NSArray *)objects
{
    // Create a gravity behavior
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:objects];
    gravity.magnitude = 10;
    return gravity;
}

- (UIDynamicBehavior *)createCollisionBehaviorForObjects:(NSArray *)objects
{
    // Create a collision behavior
    return [[UICollisionBehavior alloc] initWithItems:objects];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
