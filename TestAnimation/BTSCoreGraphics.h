//
//  Copyright (c) 2011 Brian Coyner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <stdio.h>

extern void BTSPrintCurrentCTM(CGContextRef context);

extern void BTSDrawPoint(CGContextRef context, CGPoint point);

//extern void BTSDrawCoordinateAxes(CGContextRef context);
