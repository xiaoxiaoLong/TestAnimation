//
//  basicSub.m
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import "basicSub.h"
#import "UIView+Shake.h"


@interface basicSub ()

@end

@implementation basicSub
@synthesize viewTest;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"basic SubView";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fade:(UIButton *)sender {
    [UIView animateWithDuration:1 animations:^{
        viewTest.alpha = 0.2;
        viewTest.backgroundColor = [UIColor redColor];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:1 animations:^{
            viewTest.alpha = 1;
            viewTest.backgroundColor =[UIColor greenColor];
        } completion:^(BOOL finished) {
            viewTest.alpha = 1;
            viewTest.backgroundColor =[UIColor greenColor];
        }];

    }];
}

//旋转 90 度
- (IBAction)rotate:(UIButton *)sender {
    CGAffineTransform t0 = CGAffineTransformMakeRotation(0);
    CGAffineTransform t = CGAffineTransformMakeRotation(M_PI_2);

    [UIView animateWithDuration:1 animations:^{
//        CGAffineTransform CGAffineTransformMakeRotation
        viewTest.transform = t;
    }
    completion:^(BOOL finished) {
        
        [UIView animateWithDuration:1 animations:^{
            //        CGAffineTransform CGAffineTransformMakeRotation
            viewTest.transform = t0;
        }
            completion:^(BOOL finished) {
                viewTest.transform = t0;
            }];
                }];
}

- (IBAction)move:(UIButton *)sender {
    CGPoint p1 = viewTest.center;
    
    [UIView animateWithDuration:1 animations:^{
        viewTest.center = self.view.center;
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            viewTest.center = p1;
            
        } completion:^(BOOL finished) {
            
            viewTest.center = p1;
        }];

        
    }];
    
}

- (IBAction)scale:(UIButton *)sender {
    
    CGPoint p1 = viewTest.center;
    CGPoint origin = viewTest.frame.origin;
    
    [UIView animateWithDuration:1.5 animations:^{
        
        viewTest.frame = CGRectMake(origin.x, origin.y, 300, 300);
        viewTest.center = p1;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:1.5 animations:^{
            
            viewTest.frame = CGRectMake(origin.x, origin.y, 120, 120);
            viewTest.center = p1;
        } completion:^(BOOL finished) {
            
            
        }];

        
    }];
    
    
}

- (IBAction)shake:(UIButton *)sender {
    [@[self.viewTest ] enumerateObjectsUsingBlock:^(UIView* obj, NSUInteger idx, BOOL *stop) {
        [obj shake:10
         withDelta:0.05
          andSpeed:5.0
    shakeDirection: ShakeDirectionHorizontal];
    }];

}

- (IBAction)all:(UIButton *)sender {
    
    CGAffineTransform t = self.viewTest.transform;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    transform = CGAffineTransformScale(transform,0.5, 0.5);
    transform = CGAffineTransformRotate(transform, M_PI /180.0 * 80);
    transform = CGAffineTransformTranslate(transform, 200, 0);
    
//    self.viewTest.layer.affineTransform = transform;
    [UIView animateWithDuration:1 animations:^{
        self.viewTest.transform = transform;
        self.viewTest.alpha = 0.2;
        self.viewTest.backgroundColor = [UIColor redColor];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            self.viewTest.transform = t;
            self.viewTest.alpha = 1;
            self.viewTest.backgroundColor = [UIColor greenColor];
        } completion:^(BOOL finished) {
//            self.viewTest.transform = t;
        }];

    }];
    
}

- (IBAction)threeD:(UIButton *)sender {
    CATransform3D t0 = viewTest.layer.transform;
    CATransform3D t1 = CATransform3DIdentity;
    t1.m34 = -1.0 / 500.0 ;
    t1 = CATransform3DRotate(t1, M_PI, 1, 0, 0);
    
    [UIView animateWithDuration:1 animations:^{
        
        self.viewTest.layer.transform = t1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            
            self.viewTest.layer.transform = t0;
        } completion:^(BOOL finished) {
            
        }];
    }];
    
    
}



@end
