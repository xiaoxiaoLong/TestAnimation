//
//  main.m
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
