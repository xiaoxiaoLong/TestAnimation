//
//  dynamic.m
//  TestAnimation
//
//  Created by Open on 1/13/15.
//  Copyright (c) 2015 open-groupe. All rights reserved.
//

#import "dynamic.h"

@interface dynamic ()
{
    UIAttachmentBehavior *attach ;

}

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic,retain) UIGravityBehavior *gravity;
@property (nonatomic,retain) UICollisionBehavior *collosion;
@property (nonatomic,retain) UIDynamicItemBehavior *itemBehaviour;
@property (nonatomic,retain) UISnapBehavior *snapBehavior;
@property (nonatomic,retain) UIPushBehavior *push;

@end

@implementation dynamic
@synthesize view1;
@synthesize push;

CGPoint center;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"dynamic";
    
    center = self.view1.center;
    self.animator =[[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    _gravity =[[UIGravityBehavior alloc] initWithItems:@[view1]];
    
    _collosion =[[UICollisionBehavior alloc] initWithItems:@[view1]];
    _collosion.translatesReferenceBoundsIntoBoundary = YES;
    
    _itemBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[view1]];
    _itemBehaviour.elasticity =0.6;
    
    
}


- (IBAction)push:(id)sender {
    // Add a gesture recogniser
    [self clear:nil];
    [_animator addBehavior:_collosion];
    

    push=[[UIPushBehavior alloc] initWithItems:@[view1] mode:UIPushBehaviorModeContinuous];
//    push.pushDirection =CGVectorMake(0, 10);
    push.pushDirection =CGVectorMake(10, 15);
    push.active=YES;
    
    [push setAngle:1.0  magnitude:5];

    [_animator addBehavior:push];
}

- (IBAction)snap:(UIButton *)sender {
    [self clear:nil];
//    [_animator addBehavior:_collosion];

    
    [self createGestureRecognizer];
    
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem:view1 snapToPoint:sender.center];
    self.snapBehavior.damping = 0.5f; /* Medium oscillation */
    [self.animator addBehavior:self.snapBehavior];


    
}
- (void) createGestureRecognizer{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
}

- (void) handleTap:(UITapGestureRecognizer *)paramTap{
    CGPoint tapPoint = [paramTap locationInView:self.view];
    
    if (self.snapBehavior != nil){
        [self.animator removeBehavior:self.snapBehavior];
    }
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem:self.view1 snapToPoint:tapPoint];
    self.snapBehavior.damping = 0.5f;  //剧列程度
    [self.animator addBehavior:self.snapBehavior];
}

- (IBAction)gravity:(id)sender {
    [_animator addBehavior:_gravity];
    [_animator addBehavior:_collosion];
    [_animator addBehavior:_itemBehaviour];
}
- (IBAction)up:(id)sender {
    push.pushDirection = CGVectorMake( 0,-10);
    
}
- (IBAction)down:(id)sender {
  
    push.pushDirection = CGVectorMake( 0,10);
}
- (IBAction)right:(id)sender {
     push.pushDirection = CGVectorMake(10,0);
}
- (IBAction)left:(id)sender {
     push.pushDirection = CGVectorMake(-10,0);
}


- (IBAction)clear:(id)sender {
    [_animator removeAllBehaviors];
    self.view1.center = center;
}



-(void)handlePan:(UIPanGestureRecognizer*)gesture
{
    CGPoint touchPoint = [gesture locationInView:self.view];
    UIView *draggedView = gesture.view;
    
    if(gesture.state ==UIGestureRecognizerStateBegan)
    {

        if(attach)
        {
            [_animator removeBehavior:attach];
            attach=nil;
        }
        attach=[[UIAttachmentBehavior alloc] initWithItem:draggedView attachedToAnchor:touchPoint];
        attach.damping = 0.5;
        attach.frequency = 1.0;
        [_animator addBehavior:attach];
        
    }
    else if(gesture.state == UIGestureRecognizerStateChanged)// && _draggedView)
    {
        [attach setAnchorPoint:touchPoint];
        NSLog(@"int the touchmeve event ! and the touchPoint is (%f , %f )",touchPoint.x,touchPoint.y);
    }else if (gesture.state == UIGestureRecognizerStateEnded)// && _draggedView)
    {
        //        _draggedView = NO;
        [_animator removeBehavior:attach];
        NSLog(@"int the end   !");
        
    }else if (gesture.state ==UIGestureRecognizerStateFailed)
    {
        NSLog(@"int the failed   !");
    }
    
}

@end
